﻿namespace ServiceGuard.Models
{
    public class AvailabilityModel
    {
        public string GuardEndpoint { get; set; }
        public string TargetService { get; set; }
        public string TargetMachine { get; set; }
        public string TargetIp { get; set; }
    }
}