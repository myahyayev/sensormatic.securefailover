﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Sensormatic.Failover.Common.Util
{
    public class OSUtility
    {
        public static Task<ServiceControllerStatus> CheckServiceAsync(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();

            if (services.Any(x => x.ServiceName == serviceName))
                return Task.FromResult(services.FirstOrDefault(x => x.ServiceName == serviceName).Status);
            else
                throw new System.Exception("Servis bilgisi bulanamadı");
        }

        public static Task<ServiceControllerStatus> StopService(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();

            if (services.Any(x => x.ServiceName == serviceName))
            {
                ServiceController service = services.FirstOrDefault(x => x.ServiceName == serviceName);

                service.Stop();

                service.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 30));

                if (service.Status == ServiceControllerStatus.Stopped)
                    Logger.WriteLog($"{service.ServiceName} servisi durduruldu");
                else
                    Logger.WriteLog($"{service.ServiceName} servisi durdurulamadı");

                return Task.FromResult(services.FirstOrDefault(x => x.ServiceName == serviceName).Status);
            }
            else
                throw new System.Exception("Servis bilgisi bulanamadı");
        }

        public static void StartWindowsService(string serviceNameToStart)
        {
            ServiceController serviceController = null;

            ServiceController[] services = ServiceController.GetServices();

            if (services.Any(x => x.ServiceName == serviceNameToStart))
            {
                serviceController = services.FirstOrDefault(x => x.ServiceName == serviceNameToStart);
            }
            else
            {
                throw new Exception($"Çalıştırılması istenilen {serviceNameToStart} servisi bulunamadı");
            }

            if (serviceController.Status == ServiceControllerStatus.Stopped || serviceController.Status == ServiceControllerStatus.StopPending)
            {
                Logger.WriteLog($"{serviceController.ServiceName} servisi durmuş");

                serviceController.Start();

                serviceController.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 30));

                if (serviceController.Status != ServiceControllerStatus.Running)
                {
                    throw new Exception($"{serviceNameToStart} servisini başlatma başarısız oldu");
                }
                else
                {
                    Logger.WriteLog($"{serviceNameToStart} servisi başarıyla çalıştırıldı");
                }
            }
            else if (serviceController.Status == ServiceControllerStatus.Running)
            {
                Logger.WriteLog($"{serviceController.ServiceName} servisi zaten çalışıyormuş");
            }
            else
            {
                Logger.WriteLog($"{serviceController.ServiceName} servisinin durumu: {serviceController.Status.ToString()}");
            }
        }
    }
}