﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace Sensormatic.Failover.Common.Util
{
    public class Logger
    {
        public static void WriteLog(Exception ex)
        {
            try
            {
                bool enableFileLogging = ConfigurationManager.AppSettings["EnableFileLogging"]?.ToString() == "1";

                if (enableFileLogging)
                {
                    string logRootPath = ConfigurationManager.AppSettings["FileLogPath"].ToString();

                    DirectoryInfo di = new DirectoryInfo(logRootPath);

                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    string path = logRootPath + "\\" + DateTime.Now.ToString("yyyy_MM_dd_HH") + ".log";

                    StringBuilder sbLog = new StringBuilder();
                    sbLog.AppendLine("====================");
                    sbLog.AppendLine(ex.Message);
                    sbLog.AppendLine();
                    if (ex.InnerException != null)
                    {
                        sbLog.AppendLine(ex.InnerException.Message);
                        sbLog.AppendLine();
                    }
                    sbLog.AppendLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                    sbLog.AppendLine("====================");

                    File.AppendAllText(path, sbLog.ToString());
                }
            }
            catch (Exception e) { }
        }

        public static void WriteLog(string message)
        {
            try
            {
                bool enableFileLogging = ConfigurationManager.AppSettings["EnableFileLogging"]?.ToString() == "1";

                if (enableFileLogging)
                {
                    string logRootPath = ConfigurationManager.AppSettings["FileLogPath"].ToString();

                    DirectoryInfo di = new DirectoryInfo(logRootPath);

                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    string path = logRootPath + "\\" + DateTime.Now.ToString("yyyy_MM_dd_HH") + ".log";

                    StringBuilder sbLog = new StringBuilder();
                    sbLog.AppendLine("====================");
                    sbLog.AppendLine(message);
                    sbLog.AppendLine();
                    sbLog.AppendLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                    sbLog.AppendLine("====================");

                    File.AppendAllText(path, sbLog.ToString());
                }
            }
            catch (Exception ex) { }
        }
    }
}