﻿using Newtonsoft.Json;
using Sensormatic.Failover.Common.Util;
using ServiceGuard.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ServiceGuard.Controllers
{
    public class ServicesController : Controller
    {
        [Route(nameof(CheckService))]
        [HttpGet]
        public async Task<JsonResult> CheckService(string serviceName)
        {
            ServiceResultModel serviceResult = new ServiceResultModel();

            try
            {
                serviceResult.IsSuccess = true;
                serviceResult.ServiceStatus = (int)await OSUtility.CheckServiceAsync(serviceName);       

                Logger.WriteLog(JsonConvert.SerializeObject(serviceResult));

                return Json(serviceResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                serviceResult.IsSuccess = false;
                serviceResult.ErrorMessage = ex.Message;

                Logger.WriteLog(ex);

                return Json(serviceResult);
            }
        }

        public async Task<JsonResult> StopService(string serviceName)
        {
            ServiceResultModel serviceResult = new ServiceResultModel();

            try
            {
                serviceResult.IsSuccess = true;
                serviceResult.ServiceStatus = (int)await OSUtility.StopService(serviceName);

                Logger.WriteLog(JsonConvert.SerializeObject(serviceResult));

                return Json(serviceResult);
            }
            catch (Exception ex)
            {
                serviceResult.IsSuccess = false;
                serviceResult.ErrorMessage = ex.Message;

                Logger.WriteLog(ex);

                return Json(serviceResult);
            }
        }
    }
}