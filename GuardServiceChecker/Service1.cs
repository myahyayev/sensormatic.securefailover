﻿using Sensormatic.Failover.Common.Helpers;
using Sensormatic.Failover.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GuardServiceChecker
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        Timer timer = null;
        bool running = false;

        protected override void OnStart(string[] args)
        {
            Logger.WriteLog($"Servis çalışmaya başlıyor");

            timer = new Timer();
            timer.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["CheckInterval"]);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        public void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Logger.WriteLog("Servis çalışmaya başladı");

            if (!running)
            {
                running = true;

                try
                {
                    Logger.WriteLog("Timer denetimi başladı");

                    ExecuteWindowsService();

                    CallUrl();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);
                }

                running = false;
            }

            Logger.WriteLog("Timer tamamlandı");
        }

        private static void CallUrl()
        {
            HttpHelper httpHelper = new HttpHelper();

            string urlWillBeCalled = ConfigurationManager.AppSettings["UrlWillBeCalled"].ToString();

            if (string.IsNullOrEmpty(urlWillBeCalled))
            {
                throw new Exception("Çağırılacak URL belirtilmemiş");
            }

            Task.Run(async delegate
            {
                try
                {
                    string result = await httpHelper.CallGetMethodAsync(urlWillBeCalled);

                    Logger.WriteLog($"{urlWillBeCalled} adresinden dönen yanıt: {result}");
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);
                }
            }).Wait();
        }

        private static void ExecuteWindowsService()
        {
            string serviceName = ConfigurationManager.AppSettings["ServiceNameWillBeChecked"].ToString();

            if (string.IsNullOrEmpty(serviceName))
            {
                throw new Exception("Denetelenecek servis adı belirtilmemiş");
            }

            ServiceController[] services = ServiceController.GetServices();

            if (services.Any(x => x.ServiceName == serviceName))
            {
                ServiceController service = services.FirstOrDefault(x => x.ServiceName == serviceName);

                if (service.Status == ServiceControllerStatus.Running)
                {
                    Logger.WriteLog($"Çalıştırılması istenilen {service.ServiceName} servisi zaten çalışıyormuş");
                }
                else
                {
                    service.Start();

                    Logger.WriteLog($"{service.ServiceName} servisi başlatılıyor");

                    service.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 30));

                    Logger.WriteLog($"{service.ServiceName} servisi başladıldı");
                }
            }
            else
            {
                throw new Exception($"Çalıştırılması istenilen {serviceName} servisi bulunamadı");
            }
        }

        protected override void OnStop()
        {
            Logger.WriteLog("Servis durduruluyor");

            if (timer != null && timer.Enabled)
            {
                timer.Stop();

                Logger.WriteLog("Timer durdu");
            }

            Logger.WriteLog("Servis kapanıyor");
        }
    }
}
