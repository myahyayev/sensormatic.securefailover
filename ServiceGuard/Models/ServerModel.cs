﻿namespace ServiceGuard.Models
{
    public class ServerModel
    {
        public string Name { get; set; }
        public string MachineName { get; set; }
        public string IpAddress { get; set; }
        public string Exception { get; set; }
    }
}