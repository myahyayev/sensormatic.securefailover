﻿using ServiceGuard.Models;
using ServiceGuard.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ServiceGuard.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        [Route(nameof(Index))]
        [Route("Home/Index")]
        public async Task<ActionResult> Index()
        {
            try
            {
                Repository repository = new Repository();

                ServerModel serverInfo = await repository.GetRunningServerInfoAsync();

                return View(serverInfo);
            }
            catch (Exception ex)
            {
                return View(new ServerModel() { Exception = ex.Message });
            }
        }
    }
}
