﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Sensormatic.Failover.Common.Helpers
{
    public class HttpHelper
    {
        public async Task<string> CallGetMethodAsync(string endpoint)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(endpoint));
            httpWebRequest.Method = "GET";

            using (WebResponse httpWebResponse = await httpWebRequest.GetResponseAsync())
            {
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    return await streamReader.ReadToEndAsync();
                }
            }
        }

        public async Task<TResult> CallGetMethodAsync<TResult>(string endpoint)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(endpoint));
            httpWebRequest.Method = "GET";
            httpWebRequest.ContentType = "application/json; charset=UTF-8";

            using (WebResponse httpWebResponse = await httpWebRequest.GetResponseAsync())
            {
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    string jsonResult = await streamReader.ReadToEndAsync();

                    return JsonConvert.DeserializeObject<TResult>(jsonResult);
                }
            }
        }

        public async Task<TResult> CallPostMethodAsync<TEntity, TResult>(string endpoint, TEntity postData)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(endpoint));
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json; charset=UTF-8";

            using (StreamWriter streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
            {
                string jsonBody = JsonConvert.SerializeObject(postData);

                await streamWriter.WriteAsync(jsonBody);

                using (WebResponse httpWebResponse = await httpWebRequest.GetResponseAsync())
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        string jsonResult = await streamReader.ReadToEndAsync();

                        return JsonConvert.DeserializeObject<TResult>(jsonResult);
                    }
                }
            }
        }
    }
}