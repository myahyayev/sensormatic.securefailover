﻿using Sensormatic.Failover.Common.Util;
using System;

namespace ServiceGuard.Models
{
    public class ServiceResultModel
    {
        public int ServiceStatus { get; set; }
        public string Ip { get; set; }
        public string ServerName { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public ServiceResultModel()
        {
            this.ServerName = Environment.MachineName;
            this.Ip = NetworkUtility.GetLocalIPAddress();
        }
    }
}