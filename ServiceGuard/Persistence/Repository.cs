﻿using Sensormatic.Failover.Common.Util;
using ServiceGuard.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ServiceGuard.Persistence
{
    public class Repository
    {
        public async Task SetDataBaseIPAsync(string machineName, string ipAddress)
        {
            Exception exception = null;

            string connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Connection string tanımlanmamış");
            }

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    using (SqlCommand sqlCommand = new SqlCommand($"UPDATE dbo.ApplicationServer SET MachineName ='{machineName}',Uri='net.tcp://{machineName}.sensorpoc.local:8999/CrossFire/IClientSession',IPAddress='{ipAddress}'", sqlConnection))
                    {
                        if (sqlConnection.State != ConnectionState.Open)
                        {
                            await sqlConnection.OpenAsync();
                        }

                        await sqlCommand.ExecuteNonQueryAsync();
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);

                    exception = ex;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }

                if (exception != null)
                {
                    throw exception;
                }
            }
        }

        public async Task<ServerModel> GetRunningServerInfoAsync()
        {
            ServerModel serverModel = new ServerModel();

            Exception exception = null;

            string connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Connection string tanımlanmamış");
            }

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    using (SqlCommand sqlCommand = new SqlCommand($"SELECT TOP 1 Name, MachineName, IPAddress FROM ApplicationServer", sqlConnection))
                    {
                        if (sqlConnection.State != ConnectionState.Open)
                        {
                            await sqlConnection.OpenAsync();
                        }

                        SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                        if (sqlDataReader.HasRows)
                        {
                            while (await sqlDataReader.ReadAsync())
                            {
                                serverModel.Name = sqlDataReader.GetString(0);
                                serverModel.MachineName = sqlDataReader.GetString(1);
                                serverModel.IpAddress = sqlDataReader.GetString(2);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);

                    exception = ex;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }

                if (exception != null)
                {
                    throw exception;
                }
            }

            return serverModel;
        }
    }
}