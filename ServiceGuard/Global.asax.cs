﻿using Newtonsoft.Json;
using Sensormatic.Failover.Common.Helpers;
using Sensormatic.Failover.Common.Util;
using ServiceGuard.Models;
using ServiceGuard.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Routing;

namespace ServiceGuard
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            CheckServiceStatusAsync();
        }

        private Task CheckServiceStatusAsync()
        {
            return Task.Run(delegate
            {
                double checkInterval = Convert.ToDouble(WebConfigurationManager.AppSettings["CheckInterval"].ToString());

                Timer timer = new Timer(checkInterval);

                bool running = false;

                timer.Elapsed += (async delegate
                {
                    if (!running)
                    {
                        try
                        {
                            running = true;

                            Logger.WriteLog($"Checkup => IP={NetworkUtility.GetLocalIPAddress()} - MachineName:{Environment.MachineName}");

                            List<ServiceResultModel> otherServiceStatuses = await CheckOtherServicesAsync();

                            Logger.WriteLog($"Diğer servis durumları: {JsonConvert.SerializeObject(otherServiceStatuses)}");

                            string[] servicesWillBeStarted = WebConfigurationManager.AppSettings["ServiceNamesWillBeStarted"].ToString().Split(';');

                            Logger.WriteLog($"Başlatılacak servisler: {JsonConvert.SerializeObject(servicesWillBeStarted)}");

                            if (!servicesWillBeStarted.Any())
                            {
                                throw new Exception("Çalıştırılacak servis/servisler belirtilmemiş");
                            }

                            if (!otherServiceStatuses.Any(x => x.IsSuccess) || !otherServiceStatuses.Any(x => x.IsSuccess && x.ServiceStatus == (int)ServiceControllerStatus.Running))
                            {
                                Logger.WriteLog($"Diğer servisler çalışmıyor veya servisler 404 dönüyor");

                                Logger.WriteLog("Bu sunucu veritabanına kaydediliyor");

                                await RegisterServerToDataBase();

                                Logger.WriteLog("Bu sunucu veritabanına kaydedildi");

                                foreach (string serviceNameToStart in servicesWillBeStarted)
                                {
                                    Logger.WriteLog($"{serviceNameToStart} windows servisi için başlatma süreci başladı");

                                    OSUtility.StartWindowsService(serviceNameToStart);

                                    Logger.WriteLog($"{serviceNameToStart} windows için başlatma süreci tamamlandı");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(ex);
                        }

                        running = false;
                    }
                });

                timer.Start();
            });
        }

        private static async Task RegisterServerToDataBase()
        {
            Repository repository = new Repository();

            await repository.SetDataBaseIPAsync(Environment.MachineName, NetworkUtility.GetLocalIPAddress());

            Logger.WriteLog("DB bilgileri güncellendi");
        }

        private async Task<List<ServiceResultModel>> CheckOtherServicesAsync()
        {
            List<ServiceResultModel> serviceResults = new List<ServiceResultModel>();

            string[] apiServices = WebConfigurationManager.AppSettings["CheckOtherServiceApiList"].ToString().Split(';');

            foreach (string apiService in apiServices)
            {
                string apiEndpoint = apiService.Trim();

                string serviceNameWillBeChecked = WebConfigurationManager.AppSettings["ServiceNameWillBeChecked"]?.ToString();

                if (string.IsNullOrEmpty(serviceNameWillBeChecked))
                {
                    throw new Exception("Denetlenecek servis adı belirtilmemiş");
                }

                apiEndpoint += "?serviceName=" + serviceNameWillBeChecked;

                serviceResults.Add(await CallApiServiceAsync(apiEndpoint));
            }

            return serviceResults;
        }

        private async Task<List<ServiceResultModel>> StopOtherServicesAsync(string serviceName)
        {
            List<ServiceResultModel> serviceResults = new List<ServiceResultModel>();

            if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["StopOtherServiceApiList"]?.ToString()))
            {
                string[] apiServices = WebConfigurationManager.AppSettings["StopOtherServiceApiList"]?.ToString().Split(';');

                foreach (string apiService in apiServices)
                {
                    string apiEndpoint = apiService.Trim();

                    if (string.IsNullOrEmpty(serviceName) || string.IsNullOrWhiteSpace(serviceName))
                    {
                        throw new Exception("Durdurulması istenecek servis adı belirtilmemiş");
                    }

                    apiEndpoint += "?serviceName=" + serviceName.Trim();

                    await CallApiServiceAsync(apiEndpoint);
                }
            }

            return serviceResults;
        }

        private async Task<ServiceResultModel> CallApiServiceAsync(string endpoint)
        {
            try
            {
                HttpHelper httpHelper = new HttpHelper();

                ServiceResultModel serviceResultModel = await httpHelper.CallGetMethodAsync<ServiceResultModel>(endpoint);

                return serviceResultModel;
            }
            catch (WebException wex)
            {
                if (wex.Response is HttpWebResponse)
                {
                    HttpWebResponse httpWebResponse = wex.Response as HttpWebResponse;

                    return new ServiceResultModel() { IsSuccess = false, ErrorMessage = wex.Message, Ip = new Uri(endpoint).Host };
                }
                else
                    return new ServiceResultModel() { IsSuccess = false, ErrorMessage = wex.Message, Ip = new Uri(endpoint).Host };
            }
            catch (Exception ex)
            {
                return new ServiceResultModel() { IsSuccess = false, ErrorMessage = ex.Message };
            }
        }
    }
}
